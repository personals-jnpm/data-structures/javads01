package models;

public class Requirements {
    private String SO;
    private String processor;
    private String memory;
    private String network;
    private String storage;

    public Requirements() {
    }

    public Requirements(String SO, String processor, String memory, String network, String storage) {
        this.SO = SO;
        this.processor = processor;
        this.memory = memory;
        this.network = network;
        this.storage = storage;
    }

    public String getSO() {
        return SO;
    }

    public void setSO(String SO) {
        this.SO = SO;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    @Override
    public String toString() {
        return "Requirements {" +
               "\n SO = " + SO +
               ",\n processor = " + processor +
               ",\n memory = " + memory +
               ",\n network = " + network +
               ",\n storage = " + storage +
               "\n}";
    }

}
