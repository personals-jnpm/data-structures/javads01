package models;

public class Game {

    private int id;
    private String name;
    private Category category;
    private String description;
    private Requirements requirements;
    private double price;

    public Game() {
    }

    public Game(String name, Category category, String description, Requirements requirements, double price) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.requirements = requirements;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Requirements getRequirements() {
        return requirements;
    }

    public void setRequirements(Requirements requirements) {
        this.requirements = requirements;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return id + ". " + name + "\n";
    }


    public String printGame() {
        return "\n╔═══════════════════════════════════════════════════════╗" +
               "\n   " + name +
               "\n════════════════════════════════════════════════════════" +
               "\n   Categoría: " + category +
               "\n   Descripción: " + description +
               "\n   Precio: $" + price +
               "\n   Requerimientos: " +
               "\n        SO: " + requirements.getSO() +
               "\n        Procesador: " + requirements.getProcessor() +
               "\n        Memoria: " + requirements.getMemory() +
               "\n        Red: " + requirements.getNetwork() +
               "\n        Almacenamiento: " + requirements.getStorage() +
               "\n╚═══════════════════════════════════════════════════════╝\n"
                ;
    }


}
