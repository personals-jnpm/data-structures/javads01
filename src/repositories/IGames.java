package repositories;

import models.Game;

import java.util.Collection;

public interface IGames {

    void add(Game game);

    Collection<Game> findAll();
    void update(int id, Game game) throws Exception;
    void delete(int id) throws Exception;
    Game findById(int id) throws Exception;
}
