package repositories;

import models.Game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GameRepository implements IGames {

    private final List<Game> games;

    public GameRepository() {
        games = new ArrayList<>();
    }

    @Override
    public void add(Game game) {
        game.setId(games.size());
        games.add(game);
    }

    @Override
    public Collection<Game> findAll() {
        return games;
    }

    @Override
    public void update(int id, Game game) throws Exception {
        validIndex(id);
        game.setId(id);
        games.set(id, game);
    }

    @Override
    public void delete(int id) throws Exception {
        validIndex(id);
        games.remove(id);
    }

    @Override
    public Game findById(int id) throws Exception {
        validIndex(id);
        return games.get(id);
    }

    private void validIndex(int id) throws Exception {
        if (games.size() <= id || id < 0) {
            throw new Exception("El id del juego no es válido");
        }
    }
}
