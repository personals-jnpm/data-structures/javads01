import controllers.GameController;
import models.Category;
import models.Game;
import models.Requirements;

import java.util.Collection;
import java.util.Scanner;

public class Main {

    static Scanner keyboard = new Scanner(System.in);
    static GameController games = new GameController();

    public static void main(String[] args) {
        int option = 0;
        menu();
        do {
            try {
                System.out.print("Seleccione una opción en el menú principal: ");
                option = readInteger();

                switch (option) {
                    case 0 -> System.out.println("El programa ha finalizado");
                    case 1 -> showGames();
                    case 2 -> selectGame();
                    case 3 -> createGame();
                    case 4 -> updateGame();
                    case 5 -> deleteGame();
                    default -> System.out.println("¡Opción no valida!");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (option != 0);
    }

    public static void menu() {
        System.out.println("╔═══════════════════════════════════════════╗");
        System.out.println("╠-----------------InfoSteam-----------------╣");
        System.out.println("║═══════════════════════════════════════════║");
        System.out.println("║   1. Mostrar los juegos                   ║");
        System.out.println("║   2. Seleccionar un juego                 ║");
        System.out.println("║   3. Crear un juego                       ║");
        System.out.println("║   4. Editar un jueg0                      ║");
        System.out.println("║   5. Eliminar un juego                    ║");
        System.out.println("║   0. Salir                                ║");
        System.out.println("╚═══════════════════════════════════════════╝\n");
    }

    public static void selectGame() throws Exception {
        System.out.print("Seleccione el índice del juego: ");
        int option = readInteger();
        System.out.println(games.findGameById(option).printGame());
    }

    public static void showGames() {
        System.out.println("\n╔════════════════════════════════════════════════╗" +
                           "\n║          Lista de juegos disponibles           ║" +
                           "\n╚════════════════════════════════════════════════╝" +
                           "\n" + printGames() +
                           "╚════════════════════════════════════════════════╝\n");
    }

    public static void createGame() {
        System.out.println("""
                ╔══════════════════════════════════════════════╗
                ╠------------------Crear juego-----------------╣
                ╚══════════════════════════════════════════════╝""");
        games.addGame(getGame());
        System.out.println("""
                ╚══════════════════════════════════════════════╝ 
                """);
    }

    public static void updateGame() throws Exception {
        System.out.println("""
                ╔══════════════════════════════════════════════╗
                ╠---------------Actualizar juego---------------╣
                ╚══════════════════════════════════════════════╝""");
        System.out.print(" Ingrese el id del juego: ");
        games.updateGame(readInteger(), getGame());
        System.out.println("""
                ╚══════════════════════════════════════════════╝ 
                """);
    }

    public static void deleteGame() throws Exception {
        System.out.print("Ingrese el id del juego que desea eliminar: ");
        games.deleteGame(readInteger());
    }

    private static Game getGame() {
        Game game = new Game();

        System.out.print(" Ingrese el nombre del juego: ");
        game.setName(keyboard.nextLine());
        game.setCategory(getCategory());
        System.out.print(" Ingrese la descripción: ");
        game.setDescription(keyboard.nextLine());
        System.out.print(" Ingrese el precio del juego: ");
        game.setPrice(readInteger());
        game.setRequirements(getRequirements());

        return game;
    }

    private static Requirements getRequirements() {
        Requirements requirements = new Requirements();

        System.out.println(" \nRequisitos del sistema:");
        System.out.print("  Ingrese el SO: ");
        requirements.setSO(keyboard.nextLine());
        System.out.print("  Ingrese el procesador recomendado: ");
        requirements.setProcessor(keyboard.nextLine());
        System.out.print("  Ingrese memoria mínima: ");
        requirements.setMemory(keyboard.nextLine());
        System.out.print("  Ingrese la red compatible: ");
        requirements.setNetwork(keyboard.nextLine());
        System.out.print("  Ingrese el almacenamiento requerido: ");
        requirements.setStorage(keyboard.nextLine());

        return requirements;
    }

    private static Category getCategory() {
        Category category = Category.SinDefinir;
        System.out.print(" Ingrese la categoría del juego: ");
        var input = (keyboard.nextLine()).toLowerCase();
        switch (input) {
            case "gratis" -> category = Category.Gratis;
            case "premium" -> category = Category.Premium;
            default -> System.out.println("   ¡Categoría no encontrada!");
        }
        return category;
    }

    private static int readInteger() {
        String numberStr = keyboard.nextLine();
        int number = 0;
        if (numberStr.matches("[0-9]+")) {
            number = Integer.parseInt(numberStr);
        }
        return number;
    }

    private static String printGames() {
        Collection<Game> allGames = games.findAllGames();
        StringBuilder stringGames = new StringBuilder();

        for (Game game : allGames) {
            stringGames.append("  ").append(game);
        }
        return stringGames.toString();
    }
}
