package controllers;

import repositories.GameRepository;
import models.Category;
import models.Game;
import models.Requirements;

import java.util.Collection;

public class GameController {

    private final GameRepository gameRepository;

    public GameController() {
        this.gameRepository = new GameRepository();
        initGames();
    }

    public void addGame(Game game) {
        gameRepository.add(game);
    }

    public void updateGame(int id, Game game) throws Exception {
        gameRepository.update(id, game);
    }

    public void deleteGame(int id) throws Exception {
        gameRepository.delete(id);
    }

    public Collection<Game> findAllGames() {
        return gameRepository.findAll();
    }

    public Game findGameById(int id) throws Exception {
        return gameRepository.findById(id);
    }

    public void initGames() {
        Requirements requirements1 = new Requirements("windows 7", "Intel(R) Core(TM) i5-2320 CPU @ 3.00",
                "6 GB de RAM", "Conexión de banda ancha a Internet", "5 GB de espacio disponible");
        Game game1 = new Game("Realm Royal", Category.Gratis,
                "¡Se el único en sobrevivir la Batalla \n   de Realm Royale! ", requirements1, 0);
        addGame(game1);

        Requirements requirements2 = new Requirements("windows, MacOs y SteamOs + Linux",
                "Intel Core i5 4-Core Processor", "4 GB de RAM", "Conexión wifi",
                "30 GB de espacio disponible");
        Game game2 = new Game("Total War: WARHAMMER III", Category.Premium,
                "Reagrupa a tus fuerzas y adéntrate en \n   el Reino del Caos", requirements2, 30000);
        addGame(game2);

        Requirements requirements3 = new Requirements("64-bit Windows 7/8.1/10",
                "AMD FX-6100/Intel i3-3220", "8 GB de RAM",
                "Conexión wifi", "55 GB de espacio disponible");
        Game game3 = new Game("STAR WARS Jedi: La Orden caída™", Category.Premium,
                "Una aventura de dimensiones galácticas \n   te espera en STAR WARS Jedi", requirements3,
                143000);
        addGame(game3);

        Requirements requirements4 = new Requirements("Windows 7 Sp1 64-bit",
                "Intel Core i7-4770K or AMD Ryzen 5", "8 GB de RAM",
                "Conexión de banda ancha a Internet", "10 GB de espacio disponible");
        Game game4 = new Game("Ancestors: The Humankind Odyssey", Category.Premium,
                "Lidera a tu clan en África hace 10 \n   millones de años durante los albores " +
                "de la humanidad", requirements4, 68000);
        addGame(game4);

        Requirements requirements5 = new Requirements("64-bit Windows 10", "Intel(R) Core(TM) i5-6400 CPU @",
                "8 GB de RAM", "Conexión wifi", "18 GB de espacio disponible");
        Game game5 = new Game("Spellbreak", Category.Gratis, "Spellbreak es un juego multijugador de \n" +
                                                           "   acción y hechizos", requirements5, 0);
        addGame(game5);
    }
}
